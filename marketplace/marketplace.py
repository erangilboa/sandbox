# marketplace/marketplace.py
import os

from flask import Flask, render_template
import grpc

from recommendations_pb2 import BookCategory, RecommendationRequest
from recommendations_pb2_grpc import RecommendationsStub

print(f'Eran ********')
app = Flask(__name__)

recommendations_host = os.getenv("RECOMMENDATIONS_HOST", "localhost")
recommendations_channel = grpc.insecure_channel(
    f"{recommendations_host}:50051"
)
recommendations_client = RecommendationsStub(recommendations_channel)
print(f'Eran ********')

@app.route("/")
def render_homepage():
    print(f'in render_homepage')
    try:
        recommendations_request = RecommendationRequest(
            user_id=1, category=BookCategory.MYSTERY, max_results=3
        )
        recommendations_response = recommendations_client.Recommend(
            recommendations_request
        )
        return render_template(
            "homepage.html",
            recommendations=recommendations_response.recommendations,
        )
    except Exception as e:
        print(f'error message is: {e}')
